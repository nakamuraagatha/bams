<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/smartfren/admin/');
define('HTTP_CATALOG', 'http://localhost/smartfren/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/smartfren/admin/');
define('HTTPS_CATALOG', 'http://localhost/smartfren/');

// DIR
define('DIR_APPLICATION', 'C:\xampp\htdocs\smartfren/admin/');
define('DIR_SYSTEM', 'C:\xampp\htdocs\smartfren/system/');
define('DIR_DATABASE', 'C:\xampp\htdocs\smartfren/system/database/');
define('DIR_LANGUAGE', 'C:\xampp\htdocs\smartfren/admin/language/');
define('DIR_TEMPLATE', 'C:\xampp\htdocs\smartfren/admin/view/template/');
define('DIR_CONFIG', 'C:\xampp\htdocs\smartfren/system/config/');
define('DIR_IMAGE', 'C:\xampp\htdocs\smartfren/image/');
define('DIR_CACHE', 'C:\xampp\htdocs\smartfren/system/cache/');
define('DIR_DOWNLOAD', 'C:\xampp\htdocs\smartfren/download/');
define('DIR_LOGS', 'C:\xampp\htdocs\smartfren/system/logs/');
define('DIR_CATALOG', 'C:\xampp\htdocs\smartfren/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'smartfren');
define('DB_PREFIX', 'oc_');
?>