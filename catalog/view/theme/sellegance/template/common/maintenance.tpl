<?php echo $header; ?>
	
	<section id="maincontent" class="row-fluid maintenance">
		<div class="span10 offset1">
			<div class="maintenance-msg">
				<?php echo $message; ?>
			</div>
		</div>
	</section>

<?php echo $footer; ?>